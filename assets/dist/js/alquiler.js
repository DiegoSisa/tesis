//alert("Si llego...");

$(function(){
//    listaAlquileres();
 //tabs
 $('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});
//para mostrar los detalles
//    function mostrar_detalles(id) {
//        $.ajax({
//            type: 'GET',
//            url: base_url + "mantenimiento/calquiler/listar_detalle/"+id,
//            dataType: "json",
//            success: function (data, textStatus, jqXHR) {
//                var html = "";
//            var i;
//          //  console.log("cliente "+data);
//            for (i = 0; i < data.length; i++) {               
//
//                    html 
//                    += "<tr>" 
//                    + "<td>" + (i+1) + "</td>" 
//                    + "<td>" + data[i].CANTIDAD_PIEZA +"</td>"                     
//                    + "<td>" + data[i].DESCRIPCION_PIEZA +"</td>" 
//                    + "<td>" + data[i].TALLA_PIEZA +"</td>" 
//                    + "<td>" + data[i].COLOR_PIEZA +"</td>"                         
//                    + "</tr>";
//                
//            }
//            $("#detalles_piezas").html(html);
//            }, 
//            error: function (jqXHR, textStatus, errorThrown) {
//                
//            }
//        });
//    }
//Para listar alquileres
  function listaAlquileres() {
     // console.log(base_url + "mantenimiento/ccliente/listarClientes");
    $.ajax({
        type: "get",
        url: base_url + "mantenimiento/calquiler/listar",
        
        dataType: "json",
        success: function(data) {
            var html = "";
            var i;
          //  console.log("cliente "+data);
            for (i = 0; i < data.length; i++) {               

                    html 
                    += "<tr>" 
                    + "<td>" + (i+1) + "</td>" 
                    + "<td>" + data[i].CEDULA_PERSONA +"</td>" 
                    + "<td>" + data[i].NOMBRE_PERSONA +' '+ data[i].APELLIDO_PERSONA + "</td>"
                    + "<td>" + data[i].FECHA_RESERVA +"</td>" 
                    + "<td>" + data[i].FECHA_RETIRO +"</td>" 
                    + "<td>" + data[i].PRECIO_TOTAL +"</td>" 
                    + "<td>" + data[i].ABONO +"</td>" 
                    + "<td>" + (data[i].PRECIO_TOTAL - data[i].ABONO) +"</td>" 
                    + "<td>" 
                    + "<span class='label label-danger'>"+data[i].ESTADO_ALQUILER+"</span>" 
                    + "</td>" 
                    + "<td>" 
                    + "<td>" + '<button type="button" data-toggle="modal" data-target="#listaDetalle" id="mostrar" onclick="return mostrar_detalles('+"'"+data[i].ID_ALQUILER+"'"+')" name="agregar" class="btn btn-info pull-right"><i class="fa fa-cart-plus" ></i>  Ver detalles</button>'+"</td>"     
                    + "</td>" 
                    + "</tr>";
                
            }
            $("#data_alquiler").html(html);
        },
        error: function(jqXHR,textStatus,errorThrown) {
            alert("No existen datos: "+ jqXHR.responseText+""+textStatus + errorThrown);
        }
    });
}

  //Funcion para traer la especialidad para editar
  $("#buscar").click(function(){
    //var id = $("#txtbusqueda").val();
   // var id = $(this).attr("txtbusqueda");
    var cedula = $("#txtbusqueda").val();
    //alert(id);
   $.ajax({
      type: "get",
      method: "post",
      url: base_url + "mantenimiento/calquiler/cargarCliente",
      //data: {id:id},
      data:{cedula:cedula},
      async: false,
      dataType: "json",
      success:function(data){
        console.log(data);
        
        if (data) {
         $("#txtcedula").val(data.CEDULA_PERSONA);
          $("#txtcliente").val(data.APELLIDO_PERSONA +" "+ data.NOMBRE_PERSONA);
          $("#txtdireccion").val(data.DIRECCION_PEROSNA);
          $("#txtcorreo").val(data.CORREO_PERSONA);
          $("#txttelefono").val(data.TELEFONO_PERSONA);
          $("#txtcel").val(data.CELULAR_PERSONA);
          $("#idcliente").val(data.ID_PERSONA);
        } else {
          var menssaje='<div class="alert alert-danger" style="font-size:10px">';
          menssaje+="El cliente no se ha registrado";
          menssaje+="</div>";
          $("#Mensaje").show();
          $("#Mensaje").html(menssaje);
          $("#Mensaje").hide(7000);
        }
      },
      fail:function(jqXHR, textStatus, errorThrown){
        alert("Sin datos: "+ textStatus);
        if (jqXHR.status === 0) {

          alert('Not connect: Verify Network.');
      
        } else if (jqXHR.status == 404) {
      
          alert('Requested page not found [404]');
      
        } else if (jqXHR.status == 500) {
      
          alert('Internal Server Error [500].');
      
        } else if (textStatus === 'parsererror') {
      
          alert('Requested JSON parse failed.');
      
        } else if (textStatus === 'timeout') {
      
          alert('Time out error.');
      
        } else if (textStatus === 'abort') {
      
          alert('Ajax request aborted.');
      
        } else {
      
          alert('Uncaught Error: ' + jqXHR.responseText);
      
        }
      }
    });
  });


});

