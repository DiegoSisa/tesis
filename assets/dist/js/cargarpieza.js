
//var base_url = "http://localhost/strajes/";
var arreglo = new Array();
var filas = 0;
var total = 0;
var totalp = 0;


function agregarpieza(id) {
  var id = id;
  $.ajax({
    type: "get",
    method: "post",
    url: base_url + "mantenimiento/calquiler/Agregar",
    data: { id: id },
    async: false,
    dataType: "json",
    success: function (data) {
      if (arreglo.includes(data.ID_PIEZA)) {
        var menssaje = '<div class="alert alert-danger" style="font-size:10px">';
        menssaje += "El cliente no se ha registrado";
        menssaje += "</div>";
        $("#Mensaje1").show();
        $("#Mensaje1").html(menssaje);
        $("#Mensaje1").hide(4000);
      } else {
        console.log(data.ID_PIEZA);
        arreglo[filas] = data.ID_PIEZA;
        var info = data;
        var 
        html = '<tr id="' + data.ID_PIEZA + '">';
        html += '<td><input type="hidden" class="input-group" name="idpiezd[]" value="'+data.ID_PIEZA+'"></td>'
        
        /*html += ''
        html += ''*/
        
        html += '<td>' + data.DESCRIPCION_PIEZA + '</td>'
        html += '<td>'
        html += '<div class="input-group col-md-6">';
        html += '<span class="input-group-btn">';
        html += '<a onclick="return cantidad(' + data.CANTIDAD_PIEZA + ',0,' + data.ID_PIEZA + ',' + data.PRECIO_PIEZA + ')" class="btn btn-danger">-</a>';
        html += '</span>'
        html += '<input type="text" name="cantidada[]" value="1" id="cant' + data.ID_PIEZA + '" name="cant" class="form-control">';
        html += '<span class="input-group-btn">';
        html += '<a onclick="return cantidad(' + data.CANTIDAD_PIEZA + ',1,' + data.ID_PIEZA + ',' + data.PRECIO_PIEZA + ')" class="btn btn-success">+</a>';
        html += '</span>'
        html += '</div></td>';
        html += '<td>' + data.CANTIDAD_PIEZA + '</td>';
        html += '<td><input type="hidden" class="input-group" name="preciounitd[]" id="preciounitd" value="'+data.PRECIO_PIEZA+'">' + data.PRECIO_PIEZA + '</td>';
        html += '<td id="totalpieza' + data.ID_PIEZA + '">' + data.PRECIO_PIEZA + '</td>';
        html += '<td> <button type="button" class="btn btn-info-sm" data-tooltip="tooltip" data-placement="top" title="Eliminar" onclick="eliminar(' + data.ID_PIEZA + ')" ><i class="fa fa-trash"></i></button></td></tr>';
        $('#detalle').append(html);
        sumar();
        filas++;
      }
    },
    fail: function (jqXHR, textStatus, errorThrown) {
      alert("Sin datos: " + textStatus);
      if (jqXHR.status === 0) {

        alert('Not connect: Verify Network.');

      } else if (jqXHR.status == 404) {

        alert('Requested page not found [404]');

      } else if (jqXHR.status == 500) {

        alert('Internal Server Error [500].');

      } else if (textStatus === 'parsererror') {

        alert('Requested JSON parse failed.');

      } else if (textStatus === 'timeout') {

        alert('Time out error.');

      } else if (textStatus === 'abort') {

        alert('Ajax request aborted.');

      } else {

        alert('Uncaught Error: ' + jqXHR.responseText);

      }
    }
  });
}

function cantidad(cant, opcion, id, precio) {
  if (opcion == 1) {
    var cont = $("#cant" + id).val();
    if (cont < cant) {
      cont++;
    }
    totalp = precio * cont;
    $("#totalpieza" + id).text(totalp);
    
    $("#cant" + id).val(cont);
   
  } else {
    var cont = $("#cant" + id).val();
    if (cont > 1) {
      cont--;
    }
    totalp = precio * cont;
    $("#totalpieza" + id).text(totalp);
    
    $("#cant" + id).val(cont);
    
  }
  sumar();


}

function eliminar(id) {
  var posicion = arreglo.indexOf("" + id);
  if (posicion !== -1) {
    arreglo.splice(posicion, 1);
  }
  $('#' + id).remove();
  sumar();
}

function sumar(){
  total = 0;
  $("#detalle tr").each(function(){
      total = total + Number($(this).find("td:eq(5)").text());
  });
  $("input[name=txttotal]").val(total.toFixed(2));
}

