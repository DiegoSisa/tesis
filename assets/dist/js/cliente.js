$(function() {
  
  listaClientes();

  //Para listar Cliente
  function listaClientes() {
     // console.log(base_url + "mantenimiento/ccliente/listarClientes");
    $.ajax({
        type: "get",
        url: base_url + "mantenimiento/ccliente/listarClientes",
        
        dataType: "json",
        success: function(data) {
            var html = "";
            var i;
          //  console.log("cliente "+data);
            for (i = 0; i < data.length; i++) {
                if (data[i].ESTADO_PERSONA == 1) {

                    html 
                    += "<tr>" 
                    + "<td>" + data[i].ID_PERSONA + "</td>" 
                    + "<td>" + data[i].NOMBRE_PERSONA +' '+ data[i].APELLIDO_PERSONA + "</td>" 
                    + "<td>" + data[i].CEDULA_PERSONA +"</td>" 
                    + "<td>" 
                    + "<span class='label label-success'>ACTIVADO</span>" 
                    + "</td>" 
                    + "<td>" 
                    + '<a href="javascript:;" class="btn btn-info info item-edit" data="' + data[i].ID_PERSONA + '">Editar</a>' 
                    + '<a href="javascript:;"class="btn btn-danger item-delete"data="' + data[i].ID_PERSONA+ '">Desactivar</a>' 
                    + "</td>" 
                    + "</tr>";
                    
                } else {

                    html 
                    += "<tr>" 
                    + "<td>" + data[i].ID_PERSONA + "</td>" 
                    + "<td>" + data[i].NOMBRE_PERSONA +' '+ data[i].APELLIDO_PERSONA + "</td>"
                    + "<td>" + data[i].CEDULA_PERSONA +"</td>" 
                    + "<td>" 
                    + "<span class='label label-danger'>DESACTIVADO</span>" 
                    + "</td>" 
                    + "<td>" 
                    + '<a href="javascript:;" class="btn btn-info info item-edit" data="' + data[i].ID_PERSONA + '">Editar</a>' 
                    + '<a href="javascript:;"class="btn btn-success item-activar"data="' + data[i].ID_PERSONA + '">Activar</a>' 
                    + "</td>" 
                    + "</tr>";
                }
            }
            $("#showdata").html(html);
        },
        error: function(jqXHR,textStatus,errorThrown) {
            alert("No existen datos: "+ jqXHR.responseText+""+textStatus + errorThrown);
        }
    });
}
});