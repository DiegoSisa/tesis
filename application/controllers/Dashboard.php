<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __Construct() {
        parent::__construct();
        $this->load->model("MTraje");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if($this->session->userdata("nombre_rol")!='ADMINISTRADOR') {
            redirect(base_url()."ccarrito");
        }
    }

    public function index() {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/dashboard');
        $this->load->view('layouts/footer');
    }

    

}
