<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ctraje extends CI_Controller {

    //Funcion para conectar con el modelo de la pieza
    public function __Construct() {
        parent::__construct();
        $this->load->model("MTraje");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if ($this->session->userdata("nombre_rol") != 'ADMINISTRADOR') {
            redirect(base_url() . "ccarrito");
        }
    }

    public function vlistart() {
        $data = array(
            'pieza' => $this->MTraje->getTraje(),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('traje/vlistar', $data);
        $this->load->view('layouts/footer');
    }

    public function modalPieza($id) {
        $data = array(
            'piezaid' => $this->MTraje->getPiezaID($id),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('traje/mdetalle', $data);
        $this->load->view('layouts/footer');
    }

    public function fadd() {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('traje/vadd');
        $this->load->view('layouts/footer');
    }

    public function guardar() {
        $codigo = $this->input->post("txtcod");
        $talla = $this->input->post("txttalla");
        $descripcion = $this->input->post("txtdescripcion");
        $color = $this->input->post("txtcolor");

        $data = array(
            'COD_PIEZA' => $codigo,
            'TALLA_PIEZA' => $talla,
            'DESCRIPCION_PIEZA' => $descripcion,
            'COLOR_PIEZA' => $color,
            'ESTADO_PIEZA' => true,
        );
        if (!empty($_FILES['fotoPieza']['name'])) {
            $upload = $this->_do_upload($codigo);
            $data['FOTO_PIEZA'] = $upload;
        } else {
            $data['FOTO_PIEZA'] = 'No disponible.png';
        }

        if ($this->MTraje->save($data)) {
            redirect(base_url() . "mantenimiento/ctraje/vlistart");
        } else {
            redirect(base_url() . "mantenimiento/ctraje/fadd");
        }
    }

    //Cargar imagen medico
    public function detalle_medico() {
        $id = $this->input->post("idMedico");
        $detalle_med = $this->MMedico->getMedicoImg($id);
        if (!$detalle_med) {
            return false;
        } else {
            echo json_encode($detalle_med);
        }
    }

    //Funcion para cambiar la foto de medico
    public function changeImage() {
        $idPersona = $this->input->post("idPersona");
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('imagen')) {
            $imagen = "";
        } else {
            $data = array('upload_data' => $this->upload->data());
            $imagen = $data["upload_data"]["file_name"];
        }

        $data_img = array(
            'FOTO_PERSONA' => $imagen,
        );

        if ($this->MPersona->update($idPersona, $data_img)) {
            redirect(base_url() . "mantenimiento/cmedico");
        } else {
            redirect(base_url() . "mantenimiento/cmedico");
        }
    }

    //metodo subir imagen
    public function _do_upload($codigo) {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 5000;
        $config['max_width'] = 2000;
        $config['max_height'] = 2000;
        $config['file_name'] = $codigo;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('fotoPieza')) {
            $error = array('error' => $this->upload->display_errors());
            $data['inputerror'][] = 'photo';
            $data['error_string'][] = 'Upload error:' . $this->upload->display_errors('', '');
            $data['status'] = false;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }

    //Funcion para cargar en la vista del cliente
    public function reservap() {

        $data = array(
            'piezas' => $this->MTraje->getPiezas(),
        );

        $this->load->view('vcliente/dashboard.php', $data);
    }

}
