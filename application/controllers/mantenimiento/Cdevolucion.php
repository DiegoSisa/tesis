<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cdevolucion extends CI_Controller {

    //Funcion para conectar con el modelo de Alquiler
    public function __Construct() {
        parent::__construct();
        $this->load->model("MTraje");
        $this->load->model("MCliente");
        $this->load->model("MAlquiler");
        $this->load->model("MDevolucion");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if($this->session->userdata("nombre_rol")!='ADMINISTRADOR') {
            redirect(base_url()."ccarrito");
        }
    }

    public function vDevolucion() {
        $data = array(
            'pieza' => $this->MTraje->getTraje(),
            'lista_alquileres' => $this->MAlquiler->listado()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('devolucion/devolucion', $data);
        $this->load->view('layouts/footer');
    }

    //Funcion para buscar datos del Cliente
    public function cargarAlquiler($id) {
        $result = $this->MAlquiler->getAlquiler($id);
        echo json_encode($result);
    }

    public function guardar() {
        $multa = $this->input->post("multa");
        $ver = true;
        if ($multa < 0) {
            $ver = false;
        }
        $data = array(
            'ID_ALQUILER' => $this->input->post("id_alquiler"),
            'OBSERVACION' => $this->input->post("observaciones"),
            'DESCRIPCION' => $this->input->post("observaciones"),
            'MULTA' => $multa,
            'MULTA_MONTO' => $ver
        );

        $this->MDevolucion->saveDevolucion($data);
        $data1 = array(
            'ESTADO_ALQUILER' => "D"
        );
        $this->MAlquiler->updateAlquiler($data1, $this->input->post("id_alquiler"));
        redirect(base_url() . "mantenimiento/cdevolucion/vDevolucion");
    }

}
