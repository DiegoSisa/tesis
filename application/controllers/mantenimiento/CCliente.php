<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ccliente extends CI_Controller {

    //Funcion para conectar con el modelo del Cliente
    public function __Construct() {
        parent::__construct();
        $this->load->model("MPersona");
        $this->load->model("MRol");
        $this->load->model("MCliente");
        $this->load->model("MCuenta");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if($this->session->userdata("nombre_rol")!='ADMINISTRADOR') {
            redirect(base_url()."ccarrito");
        }
    }

    public function vlistarc() {
        $data['active'] = "clienteActivo";
        $data['listado'] = $this->MCliente->getClientes();
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('cliente/vcliente', $data);
        $this->load->view('layouts/footer');
    }

    public function listarClientes() {
        $result = $this->MCliente->getClientes();
        echo json_encode($result);
    }

    public function guardar() {
        //DATOS PERSONA
        $cedula = $this->input->post("cedula");
        $apellidos = $this->input->post("txtapellido");
        $nombres = $this->input->post("txtnombre");
        $fechanacimiento = $this->input->post("fechaCli");
        $telefono = $this->input->post("txttelefono");
        $email = $this->input->post("txtcorreo");
        $celular = $this->input->post("txtcelular");
        $sexo = $this->input->post("sexo");
        $direccion = $this->input->post("txtdireccion");
        $rol = $this->input->post("txtrol");
        //$result = $this->MRol->getRol($rol);
        //daTOS DE CUENTA

        $data = array(
            'APELLIDO_PERSONA' => $apellidos,
            'NOMBRE_PERSONA' => $nombres,
            'CEDULA_PERSONA' => $cedula,
            'FECHA_NACIMIENTO' => $fechanacimiento,
            'TELEFONO_PERSONA' => $telefono,
            'CORREO_PERSONA' => $email,
            'ESTADO_PERSONA' => true,
            'ROL_ID' => $rol,
            'SEXO_PERSONA' => $sexo,
            'CELULAR_PERSONA' => $celular,
            'DIRECCION_PEROSNA' => $direccion,
        );

        if ($this->MPersona->save($data)) {
            $idPersona = $this->MPersona->recuperarId();
            $username = $this->input->post("usser");
            $clave = $this->input->post("txtclave");
            $dataCuenta = array(
                'USUARIO_CUENTA' => $username,
                'PASSWORD_CUENTA' => $clave,
                'ESTADO_CUENTA' => true,
                'PERSONA_ID' => $idPersona,
            );
        };
        if ($this->MCuenta->save($dataCuenta)) {
            redirect(base_url() . "mantenimiento/cusuario/vlistaU");
        } else {
            
        }
    }

    public function guardarc() {

        $cedula = $this->input->post("cedula");
        $apellidos = $this->input->post("txtapellido");
        $nombres = $this->input->post("txtnombre");
        $fechanacimiento = $this->input->post("fechaCli");
        $telefono = $this->input->post("txttelefono");
        $email = $this->input->post("txtcorreo");
        $celular = $this->input->post("txtcelular");
        $sexo = $this->input->post("txtsexo");
        $direccion = $this->input->post("txtdireccion");
        $rol = $this->MRol->getRol("CLIENTE");
        
        $data = array(
            'APELLIDO_PERSONA' => $apellidos,
            'NOMBRE_PERSONA' => $nombres,
            'CEDULA_PERSONA' => $cedula,
            'FECHA_NACIMIENTO' => $fechanacimiento,
            'TELEFONO_PERSONA' => $telefono,
            'CORREO_PERSONA' => $email,
            'ESTADO_PERSONA' => true,
            'SEXO_PERSONA' => $sexo,
            'CELULAR_PERSONA' => $celular,
            'DIRECCION_PEROSNA' => $direccion,
            'ROL_ID' => $rol->ID_ROL,
        );
        
        if ($this->MPersona->save($data)) {
            $idPersona = $this->MPersona->recuperarId();
            $username = $cedula;
            $clave = $cedula;
            $dataCuenta = array(
                'USUARIO_CUENTA' => $username,
                'PASSWORD_CUENTA' => $clave,
                'ESTADO_CUENTA' => true,
                'PERSONA_ID' => $idPersona,
            );
            $this->MCuenta->save($dataCuenta);
             redirect(base_url() . "mantenimiento/ccliente/vlistarc");
        } else {
            redirect(base_url() . "mantenimiento/ccliente/vlistarc");
        }

        /*if ($this->MPersona->save($data)) {
            redirect(base_url() . "mantenimiento/ccliente/vlistarc");
        } else {
            redirect(base_url() . "mantenimiento/ccliente/vlistarc");
        }*/
    }

}
