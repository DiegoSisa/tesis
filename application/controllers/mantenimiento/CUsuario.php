<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cusuario extends CI_Controller {

    //Funcion para conectar con el modelo
    public function __Construct() {
        parent::__construct();
        $this->load->model("MRol");
        $this->load->model("MPersona");
        $this->load->model("MCuenta");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if ($this->session->userdata("nombre_rol") != 'ADMINISTRADOR') {
            redirect(base_url() . "ccarrito");
        }
    }

    public function fadd() {
        $data = array(
            'roles' => $this->MRol->getRoles(),
            'active' => 'usuarioActivo'
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('usuario/vadd', $data);
        $this->load->view('layouts/footer');
    }

    public function newuss() {
        $data = array(
            'roles' => $this->MRol->getRoles(),
            'active' => 'usuarioActivo'
        );
        $this->load->view('admin/modaluss', $data);
    }

    public function fedit($id) {
        $data = array(
            'persona' => $this->MPersona->getPersonas($id),
            'roles' => $this->MRol->getRoles(),
        );
        $this->load->view('usuario/vedit', $data);
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('layouts/footer');
    }

    public function vlistaU() {
        $data = array(
            'persona' => $this->MPersona->getPersona(),
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('usuario/vlistar', $data);
        $this->load->view('layouts/footer');
        //print.array($data);
    }

    public function guardar() {
        //DATOS PERSONA
        $cedula = $this->input->post("cedula0");
        $apellidos = $this->input->post("txtApellido");
        $nombres = $this->input->post("txtNombres");
        $fechanacimiento = $this->input->post("fecha");
        $telefono = $this->input->post("txtTelefono");
        $email = $this->input->post("txtCorreo");
        $celular = $this->input->post("txtCelular");
        $sexo = $this->input->post("sexo");
        $direccion = $this->input->post("txtDireccion");
        $rol = $this->input->post("txtrol");

        $idPersona = $this->MPersona->recuperarId();
        $username = $this->input->post("usser");
        $clave = $this->input->post("txtclave");

        $this->form_validation->set_rules("cedula", "Cedula", "required|is_unique[persona.CEDULA_PERSONA]");

        if ($this->form_validation->run()) {
            $data = array(
                'APELLIDO_PERSONA' => $apellidos,
                'NOMBRE_PERSONA' => $nombres,
                'CEDULA_PERSONA' => $cedula,
                'FECHA_NACIMIENTO' => $fechanacimiento,
                'TELEFONO_PERSONA' => $telefono,
                'CORREO_PERSONA' => $email,
                'ESTADO_PERSONA' => true,
                'ROL_ID' => $rol,
                'SEXO_PERSONA' => $sexo,
                'CELULAR_PERSONA' => $celular,
                'DIRECCION_PEROSNA' => $direccion,
            );

            if ($this->MPersona->save($data)) {
                $dataCuenta = array(
                    'USUARIO_CUENTA' => $username,
                    'PASSWORD_CUENTA' => $clave,
                    'ESTADO_CUENTA' => true,
                    'PERSONA_ID' => $idPersona,
                );
            };
            if ($this->MCuenta->save($dataCuenta)) {
                redirect(base_url() . "mantenimiento/cusuario/vlistaU");
            } else {
                redirect(base_url() . "mantenimiento/cusuario/vadd");
            }
        } else {
            $this->fadd();
        }
    }

}
