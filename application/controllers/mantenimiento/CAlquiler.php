<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calquiler extends CI_Controller {

    //Funcion para conectar con el modelo de Alquiler
    public function __Construct() {
        parent::__construct();
        $this->load->model("MTraje");
        $this->load->model("MCliente");
        $this->load->model("MAlquiler");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        } else if ($this->session->userdata("nombre_rol") != 'ADMINISTRADOR') {
            redirect(base_url() . "ccarrito");
        }
    }

    public function vAlquiler() {
        $data = array(
            'pieza' => $this->MTraje->getTraje(),
            'lista_alquileres' => $this->MAlquiler->listado()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('alquiler/alquiler', $data);
        $this->load->view('layouts/footer');
    }

    //Funcion para buscar datos del Cliente
    public function cargarCliente() {
        $result = $this->MCliente->getCliente();
        echo json_encode($result);
    }

    //Funcion para agregar la pieza al detalle
    public function Agregar() {
        $result = $this->MTraje->getPieza();
        echo json_encode($result);
    }

    //Funcion para guardar datos del Alquiler
    public function guardaAlquiler() {
        $idclientep = $this->input->post("idcliente");
        $fechare = $this->input->post("fechares");
        $fechaal = $this->input->post("fechaalq");
        $total = $this->input->post("txttotal");
        $diferencia = $this->input->post("txtdiferencia");
        $abono = $this->input->post("txtabono");
        $documento = $this->input->post("txtdocumento");
        $observacion = $this->input->post("txtobs");
        //$estado = $this->input->post("txtestado");

        //Deetalle
        $idpieza = $this->input->post("idpiezd");
        $preciouni = $this->input->post("preciounitd");
        $cantidad = $this->input->post("cantidada");

        $data = array(
            'PERSONA_ID' => $idclientep,
            'FECHA_RESERVA' => $fechare,
            'FECHA_RETIRO' => $fechaal,
            'PRECIO_TOTAL' => $total,
            'DIFERENCIA' => $diferencia,
            'ABONO' => $abono,
            'DOCUMENTO' => $documento,
            'ESTADO_ALQUILER' => "A",
            'OBSERVACION' => $observacion,
        );

        if ($this->MAlquiler->saveAlquiler($data)) {
            $ialquiler = $this->MAlquiler->recuperaID();
            $this->save_detalle($ialquiler, $idpieza, $preciouni, $cantidad);
            redirect(base_url() . "mantenimiento/calquiler/vAlquiler");
        } else {
            redirect(base_url() . "mantenimiento/ccliente/vlistarc");
        }
    }

    protected function save_detalle($ida, $idp, $preuni, $cantp) {
        for ($i = 0; $i < count($idp); $i++) {
            $data = array(
                'ID_ALQUILER' => $ida,
                'ID_TRAJE' => $idp[$i],
                'COSTO_UNITARIO' => $preuni[$i],
                'COSTO_TOTAL' => $cantp[$i] * $preuni[$i],
                'CANTIDAD' => $cantp[$i]
            );

            $this->MAlquiler->save_det($data);
        }
    }

    public function listar() {
        $result = $this->MAlquiler->listado();
        echo json_encode($result);
    }

    public function listar_detalle($id) {
        $result = $this->MAlquiler->listado_detalle($id);
        echo json_encode($result);
    }

    public function getReserva($id) {
        $result = $this->MAlquiler->getAlquiler($id);
        echo json_encode($result);
    }

    public function levantarReserva() {
        $idalquler = $this->input->post("id_alquiler");
        $total = $this->input->post("totalL");
        $abono = $this->input->post("abonoL");
        $pendiente = $total - $abono;
        $data = array(
            'DIFERENCIA' => $pendiente,
            'ABONO' => $abono,
            'ESTADO_ALQUILER' => 'P'
        );
        $this->MAlquiler->updateAlquiler($data, $idalquler);
        redirect(base_url() . "index.php/mantenimiento/calquiler/vAlquiler");
    }

}
