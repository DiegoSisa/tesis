<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ccarrito extends CI_Controller {

    public function __Construct() {
        parent::__construct();
        $this->load->model("MTraje");
        $this->load->model("MCliente");

        $this->load->model("MAlquiler");
        if (!$this->session->userdata("login")) {
            redirect(base_url());
        }
    }

    public function index() {
        $data = array(
            'piezas' => $this->MTraje->getPiezas(),
        );
        $this->load->view('layouts/cliente/header');
        $this->load->view('layouts/cliente/body');
        $this->load->view('vcliente/dashboard', $data);
        $this->load->view('layouts/cliente/footer');

        //$this->load->view('vcliente/reserva', $data);
    }

    public function agregar_carrito($id) {
        $pos = $this->verificar($id);
        $aux = $_SESSION["carrito"];
        if ($pos >= 0) {

            $item = $aux[$pos];
            $item["cant"] = $item["cant"] + 1;
            $item["tot"] = $item["cant"] * $item["pu"];
            $aux[$pos] = $item;
            $_SESSION["carrito"] = $aux;
        } else {
            $traje = $this->MTraje->getPiezaID($id);
            $item = array("id" => $traje->ID_PIEZA, "descripcion" => $traje->DESCRIPCION_PIEZA . ' Talla: ' . $traje->TALLA_PIEZA . ' Color: ' . $traje->COLOR_PIEZA, "cant" => 1, "pu" => $traje->PRECIO_PIEZA, "tot" => $traje->PRECIO_PIEZA, "foto" => $traje->FOTO_PIEZA);
            $aux[] = $item;
            $_SESSION["carrito"] = $aux;
        }
        $items = array("nro" => count($_SESSION["carrito"]), "carrito" => $_SESSION["carrito"]);
        echo json_encode($items);
    }

    public function quitar_carrito($id) {
        $pos = $this->verificar($id);
        $aux = $_SESSION["carrito"];
        if ($pos >= 0) {

            $item = $aux[$pos];
            $item["cant"] = $item["cant"] - 1;
            if ($item["cant"] <= 0) {
                $carrito_new = array();
                for ($i = 0; $i < count($_SESSION["carrito"]); $i++) {
                    $car = $_SESSION["carrito"][$i];
                    if ($car["id"] != $item["id"]) {
                        $itemN = array("id" => $car["id"], "descripcion" => $car["descripcion"], "cant" => $car["cant"], "pu" => $car["pu"], "tot" => $car["tot"], "foto" => $car["foto"]);
                        $carrito_new[] = $itemN;
                    }
                }
                $_SESSION["carrito"] = $carrito_new;
            } else {
                $item["tot"] = $item["cant"] * $item["pu"];
                $aux[$pos] = $item;
                $_SESSION["carrito"] = $aux;
            }
        }
        $items = array("nro" => count($_SESSION["carrito"]), "carrito" => $_SESSION["carrito"]);
        echo json_encode($items);
    }

    public function verNroItems() {
        $item = array("nro" => count($_SESSION["carrito"]), "carrito" => $_SESSION["carrito"]);
        echo json_encode($item);
    }

    private function verificar($id) {
        $band = -1;
        for ($i = 0; $i < count($_SESSION["carrito"]); $i++) {
            $aux = $_SESSION["carrito"];
            $item = $aux[$i];
            if ($item["id"] == $id) {
                $band = $i;
                break;
            }
        }
        return $band;
    }

    public function verCarrito() {

        $this->load->view('layouts/cliente/header');
        $this->load->view('layouts/cliente/body');
        $this->load->view('vcliente/carrito');
        $this->load->view('layouts/cliente/footer');

        //$this->load->view('vcliente/reserva', $data);
    }

    public function guardar() {
        $idcliente = $this->input->post("id");
        $cliente = $this->MCliente->getPersonaCuenta($idcliente);
        $fecha_actual = date("Y-m-d");
//sumo 1 día
        $fecha_r = date("Y-m-d", strtotime($fecha_actual . "+ 2 days"));
        $total = 0.0;
        $carrito = $_SESSION["carrito"];
        for ($i = 0; $i < count($_SESSION["carrito"]); $i++) {
            $item = $carrito[$i];
            $total = $total + $item["tot"];
            //$itemN = array("id" => $car["id"], "descripcion" => $car["descripcion"], "cant" => $car["cant"], "pu" => $car["pu"], "tot" => $car["tot"], "foto" => $car["foto"]);
        }

        $data = array(
            'PERSONA_ID' => $cliente->ID_PERSONA,
            'FECHA_RESERVA' => $fecha_actual,
            'FECHA_RETIRO' => $fecha_r,
            'PRECIO_TOTAL' => $total,
            'DIFERENCIA' => $total,
            'ABONO' => 0.0,
            'DOCUMENTO' => "CEDULA",
            'ESTADO_ALQUILER' => "R",
            'OBSERVACION' => "RESERVA",
        );

        if ($this->MAlquiler->saveAlquiler($data)) {
            $ialquiler = $this->MAlquiler->recuperaID();
            for ($i = 0; $i < count($_SESSION["carrito"]); $i++) {
                $item = $carrito[$i];
                $data1 = array(
                    'ID_ALQUILER' => $ialquiler,
                    'ID_TRAJE' => $item["id"],
                    'COSTO_UNITARIO' => $item["pu"],
                    'COSTO_TOTAL' => $item["tot"],
                    'CANTIDAD' => $item["cant"]
                );

                $this->MAlquiler->save_det($data1);
                //    $item = $carrito[$i];
                //   $total = $total + $item["tot"];
                //$itemN = array("id" => $car["id"], "descripcion" => $car["descripcion"], "cant" => $car["cant"], "pu" => $car["pu"], "tot" => $car["tot"], "foto" => $car["foto"]);
            }
            $_SESSION["carrito"] = array();
            $items = array("nro" => count($_SESSION["carrito"]), "carrito" => $_SESSION["carrito"]);
            echo json_encode($items);
            //$this->save_detalle($ialquiler, $idpieza, $preciouni, $cantidad);
            // redirect(base_url() . "mantenimiento/calquiler/vAlquiler");
        } else {
            //redirect(base_url() . "mantenimiento/ccliente/vlistarc");
            $items = array("nro" => count($_SESSION["carrito"]), "carrito" => $_SESSION["carrito"]);
            echo json_encode($items);
        }
    }

}
