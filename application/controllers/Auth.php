<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{


	public function __construct()
	{
		parent::__construct();
		$this->load->model("MCuenta");
		$this->load->model("MRol");
	}

	public function index()
	{
		
		if ($this->session->userdata("login")) {
			redirect(base_url()."dashboard");
		} else {
			$data = array(
				'roles' => $this->MRol->getRoles(),
			);
			$this->load->view('admin/login1',$data);
		}
	}

	public function login(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$res = $this->MCuenta->login($username, ($password));
		if (!$res) {
			$this->session->set_flashdata("error", "El Usuario y/o contreseña es incorrecta...");
			redirect(base_url());
		} else {
			$data =array(
				'id' => $res->ID_CUENTA,
				'apellidos' => $res->APELLIDO_PERSONA,
				'nombres'=> $res->NOMBRE_PERSONA,
				'loginc' => $res->USUARIO_CUENTA,
				'nombre_rol' => $res->NOMBRE_ROL,
				'abreviatura_rol' => $res->ABREVIATURA_ROL,
				'sexo' => $res->SEXO_PERSONA,
				'rol' => $res->ROL_ID,
				'login' => true,
			);
			$this->session->set_userdata($data);
                        if ($this->session->userdata("login")) {
                            $_SESSION["carrito"] = array();
                            if ($this->session->userdata("nombre_rol") == "ADMINISTRADOR") {
                                redirect(base_url()."dashboard");
                            } else {
                                redirect(base_url()."ccarrito");
                            }
                        }
			
		}
		

	}
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url());
		}
}