<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MAlquiler extends CI_Model {

    //Funcion para ingresar los datos del alquiler
    public function saveAlquiler($data) {
        return $this->db->insert("alquiler", $data);
    }

    //Funcion para recuperar el ID del alquiler
    public function recuperaID() {
        return $this->db->insert_id();
    }

    //Funcion para ingresar datos del detalle
    public function save_det($data) {
        $this->db->insert("detalle_alquiler", $data);
    }

    //Funcion para traer todos los datos del cliente
    public function listado() {
        $this->db->select('alquiler.*, persona.APELLIDO_PERSONA, persona.NOMBRE_PERSONA, persona.CEDULA_PERSONA');
        $this->db->from('alquiler');
        $this->db->join('persona', 'persona.ID_PERSONA = alquiler.PERSONA_ID');
        $this->db->where('ESTADO_ALQUILER', 'P');
        $this->db->or_where('ESTADO_ALQUILER', 'R');
        //$this->db->order_by('ID_ALQUILER', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    //Funcion para traer todos los datos del cliente
    public function listado_detalle($id_alquiler) {
        $this->db->select('detalle_alquiler.*, pieza.DESCRIPCION_PIEZA, pieza.TALLA_PIEZA, pieza.COLOR_PIEZA, pieza.CANTIDAD_PIEZA');
        $this->db->from('detalle_alquiler');
        $this->db->join('pieza', 'pieza.ID_PIEZA = detalle_alquiler.ID_TRAJE');
        $this->db->where('detalle_alquiler.ID_ALQUILER', $id_alquiler);
        //$this->db->order_by('ID_ALQUILER', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    //Funcion para traer todos los datos del cliente
    public function getAlquiler($id) {
        $this->db->select('alquiler.*, persona.APELLIDO_PERSONA, persona.NOMBRE_PERSONA, persona.CEDULA_PERSONA');
        $this->db->from('alquiler');
        $this->db->join('persona', 'persona.ID_PERSONA = alquiler.PERSONA_ID');
        $this->db->where('alquiler.ID_ALQUILER', $id);
        //$this->db->order_by('ID_ALQUILER', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function updateAlquiler($data, $id_alquiler) {
        $this->db->where('ID_ALQUILER', $id_alquiler);
        $this->db->update('alquiler', $data);         
    }
    
}
