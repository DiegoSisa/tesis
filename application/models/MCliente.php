<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MCliente extends CI_Model {

    //Funcion para traer todos los datos del cliente
    public function getClientes() {
        $this->db->order_by('NOMBRE_PERSONA', 'asc');
        $query = $this->db->get('persona');
        return $query->result();
    }

    //Funcion para busca cliente deacuerdo a la cedula
    public function getCliente() {
        $cedula1 = $this->input->post('cedula');
        $this->db->select('persona.*');
        $this->db->where('CEDULA_PERSONA', $cedula1);
        $query = $this->db->get('persona');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    
    public function getPersonaCuenta($id_cuenta) {
        
        $this->db->select('persona.*');
        $this->db->from('cuenta');
        $this->db->join('persona', 'cuenta.PERSONA_ID = persona.ID_PERSONA');
        $this->db->where('ID_CUENTA', $id_cuenta);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

}
