<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPersona extends CI_Model{

	//Funcion para guardar los datos en persona
	public function save($data){
		return $this->db->insert("persona",$data);
	}
  
  //Fincion para traer el ID ingresado de pa persona
	public function recuperarId(){
		return $this ->db->insert_id();
	}
  
  //Funcion para traer todos los datos de la persona segun su rol
	public function getPersona()
  {
    $this->db->select('persona.*, rol.*');
    $this->db->from('persona');
    $this->db->join('rol', 'rol.ID_ROL = persona.ROL_ID');
    $this->db->order_by('persona.APELLIDO_PERSONA', 'asc');
    $query = $this->db->get();
    return $query->result();
	}
  
  //Funcion para traer todos los datos de la persona segun su ID
	public function getPersonas($id)
  {
    $this->db->select('persona.*');
    $this->db->from('persona');
    $this->db->where('persona.ID_PERSONA', $id);
    $resultado = $this->db->get();
    return $resultado->row();
  }
}