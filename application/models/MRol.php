<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRol extends CI_Model {

  //Funcion para tarer el rol deacuerdo al nombre
  public function getRol($abre){
    $this->db->where('NOMBRE_ROL',$abre);
    $result = $this->db->get('rol');
    return $result->row();
  }

  //Funcion para traer todos los datos del Rol
  public function getRoles()
	{
        $this->db->select('rol.*');
        $this->db->from('rol');
        $this->db->order_by('NOMBRE_ROL', 'asc');
        $query = $this->db->get();
        return $query->result();
  }
}