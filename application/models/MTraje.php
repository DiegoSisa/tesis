<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MTraje extends CI_Model {

    //Funcion para guardar datos de la pieza
    public function save($data) {
        return $this->db->insert("pieza", $data);
    }

    //Funcion recuperar el ID de la pieza
    public function recuperarId() {
        return $this->db->insert_id();
    }

    //Funcion para traer la pieza deacuerdo del estado
    public function getTraje() {
        $this->db->select('pieza.*');
        $this->db->where("pieza.ESTADO_PIEZA", true);
        $this->db->from('pieza');
        $query = $this->db->get();
        return $query->result();
    }

    //Funcion para Traer todas las piezas segun su ID
    public function getPieza() {
        $id = $this->input->post('id');
        $this->db->select('pieza.*');
        $this->db->where('ID_PIEZA', $id);
        $this->db->from('pieza');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    
    

    //Funcion para Traer los datos de la pieza segun su ID
    public function getPiezaID($id) {
        $this->db->select('pieza.*');
        $this->db->where("ID_PIEZA", $id);
       // $this->db->where("pieza.ESTADO_PIEZA", true);
        $this->db->from('pieza');
        $resultado = $this->db->get();
        return $resultado->row();
    }

    public function getPiezas() {
        $this->db->select('pieza.*');
        $this->db->from('pieza');
        $this->db->order_by('pieza.COD_PIEZA', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

}
