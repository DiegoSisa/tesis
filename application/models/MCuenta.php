<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCuenta extends CI_Model{

  //Funcion para iniciar sesión
	public function login($username, $password)
	{
    $this->db->select('cuenta.*, persona.*, rol.*');
    $this->db->from('cuenta');
    $this->db->join('persona','persona.ID_PERSONA = cuenta.PERSONA_ID');
    $this->db->join('rol', 'rol.ID_ROL = persona.ROL_ID');
    $this->db->where("cuenta.ESTADO_CUENTA",true);
    $this->db->where("cuenta.USUARIO_CUENTA",$username);
    $this->db->where("cuenta.PASSWORD_CUENTA",$password);
    $query =$this->db->get();
    if ($query->num_rows()==1) {
      return $query->row();
    } else {
      return false;
    }
    
	}
  
  //Funcion para insertar los datos de la Cuenta
  public function save($data){
    return $this->db->insert("cuenta",$data);
  }
    
}