
<div class="buy-now">
    <script>
        jQuery(document).ready(function () {
            cargarTabla();
        });
        function cargarTabla() {
            $.ajax({
                url: base_url + "ccarrito/verNroItems/",
                type: 'GET',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    //console.log(data.nro);
                    $("#nro_carro").html(data.nro);
                    dibujarTabla(data.carrito);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        }
        function dibujarTabla(data) {
            var html = "";
            var total = 0.0;
            $.each(data, function (index, item) {
                //console.log(item.descripcion);
                html += '<tr>';
                html += '<td><a class="btn btn-success" onclick="anadir_carrito(' + "'" + item.id + "'" + ')"> + </a><a href="#" ><span class="badge">' + item.cant + '</span></a><a class="btn btn-danger" onclick="quitar_carrito(' + "'" + item.id + "'" + ')"> - </a></td>';
                html += '<td>' + item.descripcion + '</td>';
                html += '<td>$ ' + item.pu + '</td>';
                html += '<td>$ ' + item.tot + '</td>';
                html += '</tr>';
                total += item.tot * 1;
            });
            html += "<tr><td colspan='4' style='text-align: right;'>TOTAL: $ " + total + "</td></tr>";
            $("#tabla tbody").html(html);
        }
        function quitar_carrito(id) {

            $.ajax({
                url: base_url + "ccarrito/quitar_carrito/" + id,
                type: 'GET',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    //console.log(data.nro);
                    $("#nro_carro").html(data.nro);
                    dibujarTabla(data.carrito);

                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        }
        function anadir_carrito(id) {

            $.ajax({
                url: base_url + "ccarrito/agregar_carrito/" + id,
                type: 'GET',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log(data.nro);
                    $("#nro_carro").html(data.nro);
                    dibujarTabla(data.carrito);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                }
            });
        }

        function guardar() {
            var cont = $("#nro_carro").html();
            cont = cont * 1;
            var r = confirm("Esta seguro de realizar la reserva! ");
            if (r == true) {
                if (cont > 0) {
                    var idcuenta = $("#cliente").val();
                    $.ajax({
                        url: base_url + "ccarrito/guardar",
                        data: 'id=' + idcuenta,
                        type: 'POST',
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            $("#nro_carro").html(data.nro);
                            dibujarTabla(data.carrito);
                            if((data.nro * 1) == 0 ) {
                                alert("Se ha realizado su reserva, tiene dos dias para hacerla efectiva");
                            }
                        }, error: function (jqXHR, textStatus, errorThrown) {

                        }
                    });
                } else {
                    alert("No se puede hacer el alquler se requiere prendas seleccionadas");
                }

            }
        }
    </script> 
    <div class="container">
        <div class="heading-setion-w3ls">
            <h3 class="title-w3layouts">Reserva de prendas <i class="fa fa-bell-o" aria-hidden="true"></i><i class="fa fa-bell" aria-hidden="true"></i></h3>
            <input type="hidden" name="cliente" value="<?php echo $this->session->userdata('id') ?>"  id="cliente">
        </div>
        <div class="team-grids">
            <!-- Bottom to top-->
            <div class="details-top-w3ls">
            </div>
            <div class="details-mid-w3ls">
                <!--inicio de espacio-->
                <table class="table table-dark" id="tabla">
                    <thead>
                        <tr>
                            <th>Cant</th>
                            <th>Descripcion</th>
                            <th>P. U</th>
                            <th>P. T</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <!--Fin de espacio-->
                <div class="modal-footer">

                    <button type="button" class="btn btn-success pull-right" onclick="guardar()">Reservar</button>
                </div>
            </div>
            <!-- end Bottom to top-->
        </div>
    </div>
</div>

<!-- //Buy-now -->
