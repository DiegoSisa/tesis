<div class="buy-now">
    <script>
        function anadir_carrito(id) {
            
            $.ajax({
                url: base_url + "ccarrito/agregar_carrito/" + id,
                type: 'GET',
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {                    
                    console.log(data.nro);
                    $("#nro_carro").html(data.nro);
                    
                },error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.responseText);
                            }
            });
        }
    </script> 
    <div class="container">
        <div class="heading-setion-w3ls">
            <h3 class="title-w3layouts">Elije la Pieza <i class="fa fa-bell-o" aria-hidden="true"></i><i class="fa fa-bell" aria-hidden="true"></i></h3>
        </div>
        <div class="team-grids">
            <!-- Bottom to top-->
            <div class="details-top-w3ls">
            </div>
            <div class="details-mid-w3ls">
                <!--inicio de espacio-->
                <?php foreach ($piezas as $pieza) : ?>
                    <div class="col-md-3 team-grid">

                        <div class="ih-item circle effect10 bottom_to_top">
                            <div class="img"><img src="<?php echo base_url(); ?>uploads/<?php echo $pieza->FOTO_PIEZA; ?>" alt="img"><span class="badge badge-danger">Stock <?php echo $pieza->CANTIDAD_PIEZA; ?></span>
                            </div>
                            <div class="info">
                                <h3>$5.00</h3>
                                <ul>
                                <!--	<li><a href="#"><i class="fa fa-retweet" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>-->
                                    <li class="cary-li-w3-agileits">
                                        <div class="snipcart-details top_brand_home_details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="Product #<?php echo $pieza->COD_PIEZA; ?>" />
                                                    <input type="hidden" name="amount" value="5.00" />
                                                    <input type="hidden" name="discount_amount" value="0.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    
                                                    <input type="button" onclick=" return anadir_carrito('<?php echo $pieza->ID_PIEZA; ?>')" value="Añadir al carrito"  class="button" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>
                <?php endforeach; ?>
                <!--Fin de espacio-->

            </div>
            <!-- end Bottom to top-->
        </div>
    </div>
</div>

<!-- //Buy-now -->
<!--footer-->
<div class="footer_bottom section">
    <div class="agileits-w3layouts-footer">
        <div class="container">
            <div class="col-md-4 w3-agile-grid">
                <h5>Acerca De Nosotros</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean bibendum urna non nisi ornare, condimentum iaculis ipsum blandit. Duis auctor vulputate metus nec luctus.</p>
                <div class="w3_agileits_social_media team_agile_w3l team footer">
                    <ul class="social-icons3">

                        <li><a href="#" class="wthree_facebook"> <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                    </ul>
                </div>	
                <div class="image-agileits">
                    <img src="images/f1.jpg" alt="" class="img-r">
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-4 w3-agile-grid mid-w3-add">
                <h5>Address</h5>
                <div class="w3-address">
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="w3-address-right">
                            <h6>Phone Number</h6>
                            <p>+1 234 567 8901</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="w3-address-right">
                            <h6>Email Address</h6>
                            <p>Email :<a href="mailto:example@email.com"> mail@example.com</a></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="w3-address-grid">
                        <div class="w3-address-left">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="w3-address-right">
                            <h6>Location</h6>
                            <p>Broome St, NY 10002, Canada. 
                                <span>Telephone : +00 111 222 3333</span>
                            </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 footer-right w3-agile-grid">
                <div class="agile_footer_grid">
                    <h5>Latest News</h5>
                    <ul class="agileits_w3layouts_footer_grid_list">
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal1">Lorem ipsum neque vulputate </a>
                        </li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal1">Dolor amet sed quam vitae</a>
                        </li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal1">Lorem ipsum neque, vulputate </a>
                        </li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal1">Dolor amet sed quam vitae</a>
                        </li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <a href="#" data-toggle="modal" data-target="#myModal1">Lorem ipsum neque, vulputate </a>
                        </li>
                    </ul>
                </div>
                <h5>Stay in Touch</h5>
                <form action="#" method="post">
                    <input type="email" name="Email" placeholder="Email Id" required="">
                    <input type="submit" value="Subscribe">
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="copyright">
        <p>© 2017 CAMILA HURTADO. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
    </div>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>CAMILA HURTADO</h4>
                <img src="images/f2.jpg" alt=" " class="img-responsive">
                <h5>Integer lorem ipsum dolor sit amet </h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, rds which don't look even slightly believable..</p>
            </div>
        </div>
    </div>
</div>
<!-- //Modal1 -->