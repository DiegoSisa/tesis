<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="Mnuevousuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header text-primary" style="background: #000000">
        <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Nuevo Usuario <i
            class="fa fa-list"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
          <div id="Mensaje1"></div>
          <!-- /.card-body -->

          <section class="content">
    <div class="row">
      <!-- Default box -->
      <div class="col-md-12">
        <div class="box box-info col-md-12">
          <div class="box-header with-border">
            
            <!-- Custom Tabs --> 
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">DATOS PERSONALES</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">DATOS DE LA CUENTA</a></li>

                
              </ul>
              <form action="<?php echo base_url();?>mantenimiento/cusuario/guardar" method="post" enctype="multipart/form-data">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <!-- Inicio 1ra fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="cedula">Cedula</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        <input type="text" class="form-control" maxlength="10" placeholder="Cedula..." id="cedula" name="txtCedula" onkeyup="validarDocumento();">
                        <?php echo form_error("cedula", "<span class='help-block'>", "</span>");?>
                        <span class="help-block"></span>
                      </div>
                      
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nombres</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Nombre..." name="txtNombres" onkeyup="mayus(this)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 1ra fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Apellidos</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Apellido..." name="txtApellido" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Telefono</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Telefono..." name="txtTelefono" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Direccion</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="	fa fa-map-marker"></i></span>
                        <input type="text" class="form-control" placeholder="Direccion..." name="txtDireccion" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Celular</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Celular..." name="txtCelular" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Correo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                        <input type="text" class="form-control" placeholder="Email..." name="txtCorreo" onkeyup="minus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Sexo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-female"></i></span>
                        <span class="input-group-addon"><i class="fa fa-male"></i></span>
                        <select name="sexo" id="sexo" class="form-control">
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6 <?php echo (form_error("fecha")) ? 'has-error' : ''; ?>">
                      <label for="fecha">Fecha de Nacimiento</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                        <input type="text" id="fecha" name="fecha" class="form-control" placeholder="aaaa-mm-dd"value="<?php echo set_value("fecha") ?>">
                      </div>
                      <?php echo form_error("fecha", "<span class='help-block'>", "</span>"); ?>
                    </div>

                   <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Rol</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <select class="form-control selecct2" style="width: 100%" name="txtrol" id="txtrol">
                        
                        <?php foreach ($roles as $rol):?> 

                             <option value="<?php echo $rol->ID_ROL?>"><?php echo $rol->NOMBRE_ROL?></option>
                          <?php endforeach;?> 
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

                <!-- Inicio Tab_2 -->
                <div class="tab-pane" id="tab_2">

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Usuario</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" placxeholder="Usuario..." name="usser">
                      </div>
                    </div>

                    <div class="form-group col-md-6">

                      <label for="textpassword">Contraseña</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" placeholder="Contraseña..." name="txtclave" id="txtclave">
                      </div><br>

                      <label for="textconfirm"> Verificar Contraseña</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" placeholder="Contraseña..." name="txtclave2" id="txtclave2" onkeyup="validacion('txtclave2')">
                      </div>
                      
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                </div>
                <!-- FIn Tab_2 -->

              </div>
              <!-- nav-tabs-custom -->

              <div class="box-footer">
                <a class="btn btn-danger" href="<?php echo base_url(); ?>mantenimiento/cusuario/vlistaU">CANCELAR</a>
                <button type="submit" class="btn btn-info pull-right">GUARDAR</button>
              </div>
              </form>
            </div>
            <div class="box-body">

            </div>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-md-8 -->
      </div>
      <!-- /.row -->
  </section>


          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>