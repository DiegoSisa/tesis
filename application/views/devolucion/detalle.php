<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="listaDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header text-primary" style="background: #000000">
        <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Lista de Piezas <i
            class="fa fa-list"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
          <div id="Mensaje1"></div>
          <!-- /.card-body -->
          <div class="card-body">
            <table id="agregarArticulo" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nro</th>                  
                  <th>Cantidad</th>
                  <th>Nombre</th>
                  <th>Talla</th>
                  <th>Color</th>
                  
                  
                </tr>
              </thead>
              <tbody id="detalles_piezas">
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>