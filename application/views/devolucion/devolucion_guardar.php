<!-- Modal -->

<div class="modal fade bs-example-modal-lg" id="devolucionGuardar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header text-primary" style="background: #000000">
                <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Devolucion <i
                        class="fa fa-list"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div id="Mensaje1"></div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <form method="POST" action="<?php echo base_url(); ?>mantenimiento/cdevolucion/guardar" id="formulario">
                            <input type="hidden" name="id_alquiler" id="id_alquiler">
                            <div class="form-group">
                                <label for="observaciones" class="col-sm-2 control-label">Observacion:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="observaciones" id="observaciones" placeholder="Observacion" >Ninguno</textarea>                                                                      
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="saldo" class="col-sm-2 control-label">Saldo:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="saldo" id="saldo" placeholder="Saldo..." onkeypress="return numeros(event)" readonly>                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="multa" class="col-sm-2 control-label">Multa:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="multa" id="multa" placeholder="Multa..." onkeypress="return numeros(event)" value="0.0">                                    
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-success pull-right">Guardar</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </div>
</div>