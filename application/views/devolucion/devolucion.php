<!-- Content Wrapper. Contains page content -->
<script>
    function mostrar_detalles(id) {
        $.ajax({
            type: 'GET',
            url: base_url + "mantenimiento/calquiler/listar_detalle/" + id,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var html = "";
                var i;
                //console.log("cliente "+data);
                for (i = 0; i < data.length; i++) {

                    html
                            += "<tr>"
                            + "<td>" + (i + 1) + "</td>"
                            + "<td>" + data[i].CANTIDAD_PIEZA + "</td>"
                            + "<td>" + data[i].DESCRIPCION_PIEZA + "</td>"
                            + "<td>" + data[i].TALLA_PIEZA + "</td>"
                            + "<td>" + data[i].COLOR_PIEZA + "</td>"
                            + "</tr>";

                }
                $("#detalles_piezas").html(html);
                console.log(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
    
    function mostrar_alquiler(id) {
        $.ajax({
            type: 'GET',
            url: base_url + "mantenimiento/cdevolucion/cargarAlquiler/" + id,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                console.log(data[0].DIFERENCIA);
                $("#saldo").val(data[0].DIFERENCIA);
                $("#id_alquiler").val(data[0].ID_ALQUILER);
                //var html = "";
                //var i;
                //console.log("cliente "+data);
                /*for (i = 0; i < data.length; i++) {

                    html
                            += "<tr>"
                            + "<td>" + (i + 1) + "</td>"
                            + "<td>" + data[i].CANTIDAD_PIEZA + "</td>"
                            + "<td>" + data[i].DESCRIPCION_PIEZA + "</td>"
                            + "<td>" + data[i].TALLA_PIEZA + "</td>"
                            + "<td>" + data[i].COLOR_PIEZA + "</td>"
                            + "</tr>";

                }*/
              //  $("#detalles_piezas").html(html);
               // console.log(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de Alquileres</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <td><b>N.</b></td>
                                <td><b>Cedula</b></td>
                                <td><b>Cliente</b></td>
                                <td><b>F. Registro</b></td>
                                <td><b>F. Retiro</b></td>                                            
                                <td><b>Total</b></td>                                            
                                <td><b>Bono</b></td>                                            
                                <td><b>Saldo pendiente</b></td>                                            
                                <td><b>Estado</b></td>
                                <td><b>Detalle</b></td>
                                <td><b>Devolver</b></td>
                                </thead>
                                <tbody id="data_alquiler">
                                    <?php
                                    $cont = 1;
                                    if (!empty($lista_alquileres)) :
                                        ?>
                                        <?php foreach ($lista_alquileres as $item) : ?>
                                            <tr>
                                                <td><?php echo $cont; ?></td>
                                                <td><?php echo $item->CEDULA_PERSONA; ?></td>
                                                <td><?php echo $item->NOMBRE_PERSONA . ' ' . $item->APELLIDO_PERSONA ?></td>
                                                <td><?php echo $item->FECHA_RESERVA; ?></td>
                                                <td><?php echo $item->FECHA_RETIRO; ?></td>
                                                <td><?php echo $item->PRECIO_TOTAL; ?></td>
                                                <td><?php echo $item->ABONO; ?></td>
                                                <td><?php echo ($item->PRECIO_TOTAL - $item->ABONO); ?></td>
                                                <td>
                                                    <span class='label label-danger'><?php echo $item->ESTADO_ALQUILER; ?></span>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" data-toggle="modal" data-target="#listaDetalle" id="mostrar" onclick="return mostrar_detalles('<?php echo $item->ID_ALQUILER; ?>')" name="agregar" class="btn btn-info pull-right"><i class="fa fa-cart-plus" ></i>  Ver detalles</button>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" data-toggle="modal" data-target="#devolucionGuardar" id="mostrarP" onclick="return mostrar_alquiler('<?php echo $item->ID_ALQUILER; ?>')" name="agregar" class="btn btn-info pull-right"><i class="fa fa-cart-plus" ></i>  Devolver</button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $cont++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php

include("detalle.php");
include("devolucion_guardar.php");
?>
