<!DOCTYPE html>
<html>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      LISTA DE USUARIOS
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active"></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-red">
          <a href="<?php echo base_url(); ?>mantenimiento/cusuario/fadd">
            <span class="info-box-icon bg-danger boton-nuevo-usuario"><i class="fa fa-user-o"></i></span>
          </a>
          <div class="info-box-content">
            <span class="info-box-text">Nuevo Usuario</span>
            <span class="info-box-number"></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- Default box -->
    <!-- Mensajes guardar y actualziar -->
    <?php if ($this->session->flashdata("exito")) : ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Éxito!</h4>
        <p><?php echo $this->session->flashdata("exito"); ?></p>
      </div>
    <?php endif; ?>
    <?php if ($this->session->flashdata("act")) : ?>
      <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Actualizado!</h4>
        <p><?php echo $this->session->flashdata("exito"); ?></p>
      </div>
    <?php endif; ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">USUARIOS REGISTRADOS</h3>
      </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Cedula</th>
                  <th>Apellidos</th>
                  <th>Nombres</th>
                  <th>Celular</th>
                  <th>Accion</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $cont = 1;
                if (!empty($persona)) : ?>
                  <?php foreach ($persona as $personas) : ?>
                    <tr>
                      <td><?php echo $cont; ?></td>
                      <td><?php echo $personas->CEDULA_PERSONA; ?></td>
                      <td><?php echo $personas->APELLIDO_PERSONA; ?></td>
                      <td><?php echo $personas->NOMBRE_PERSONA; ?></td>
                      <td><?php echo $personas->CELULAR_PERSONA; ?></td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-info" data-toggle="tooltip" title="Editar Usuario"><a href="<?php echo base_url(); ?>mantenimiento/cusuario/fedit/<?php echo $personas->ID_PERSONA;?>"><i class="fa fa-edit"></i></a></button>
                          <button type="button" class="btn btn-danger" data-toggle="tooltip" title="Desactivar paciente"><i class="fa fa-remove"></i></button>
                        </div>
                      </td>
                    </tr>
                    <?php $cont++; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
              
            </table>
          </div>
          <!-- /.box-body -->
       
        <!-- /.box -->

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->



  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

</html>