<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      USUARIO
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <!--<li><a href="<?php echo base_url();?>mantenimiento/cpaciente/index">Listar</a></li>-->
      <li class="active">Nuevo Paciente</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Default box -->
      <div class="col-md-8">
        <div class="box box-info col-md-6">
          <div class="box-header with-border">
            <h3 class="box-title">Datos del Usuario</h3>
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">DATOS PERSONALES</a></li>
                

                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
              </ul>
              <form action="<?php echo base_url();?>mantenimiento/cusuario/guardar" method="post" enctype="multipart/form-data">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <!-- Inicio 1ra fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="txtCedula">Cedula</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        <input type="text" class="form-control" name="txtCedula" id="txtCedula" value="<?php echo $persona->CEDULA_PERSONA; ?>" readonly>
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="txtnombres">Nombres</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Nombre..." name="txtNombres" value="<?php echo $persona->NOMBRE_PERSONA; ?>" onkeyup="mayus(this)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 1ra fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="Apellido">Apellidos</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Apellido..." name="txtApellido" value="<?php echo $persona->APELLIDO_PERSONA; ?>" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="txtTelefono">Telefono</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Telefono..." name="txtTelefono" value="<?php echo $persona->TELEFONO_PERSONA; ?>">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="txtDireccion">Direccion</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="	fa fa-map-marker"></i></span>
                        <input type="text" class="form-control" placeholder="Direccion..." name="txtDireccion" value="<?php echo $persona->DIRECCION_PEROSNA; ?>" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="txtCelular">Celular</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Celular..." name="txtCelular" value="<?php echo $persona->CELULAR_PERSONA; ?>">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="txtCorreo">Correo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                        <input type="text" class="form-control" placeholder="Email..." name="txtCorreo" value="<?php echo $persona->CORREO_PERSONA; ?>" onkeyup="minus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="sexo">Sexo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-female"></i></span>
                        <span class="input-group-addon"><i class="fa fa-male"></i></span>
                        <select name="sexo" id="sexo" class="form-control">
                        <?php if ($persona->SEXO_PERSONA=="M") : ?>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                          <?php else : ?>
                            <option value="F">Femenino</option>
                            <option value="M">Masculino</option>
                          <?php endif; ?>
                          
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">

                  <div class="form-group col-md-6">
                    <div class="form-group">
                      <label>FECHA NACIMIENTO</label>
                      <input type="date" class="form-control" id="txtFechNac" placeholder="Fecha Nacimiento" name="txtFechNac" value="<?php echo $persona->FECHA_NACIMIENTO; ?>">
                    </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label for="txtrol">Rol</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <select class="form-control selecct2" style="width: 100%" name="txtrol" id="txtrol">
                        
                        <?php foreach ($roles as $rol):?> 
                          <?php if ($rol->ID_ROL == $persona->ROL_ID) : ?>
                              <option value="<?php echo $rol->ID_ROL?>" selected><?php echo $rol->NOMBRE_ROL?></option>
                          <?php else : ?>
                              <option value="<?php echo $rol->ID_ROL?>"><?php echo $rol->NOMBRE_ROL?></option>
                          <?php endif; ?>
                        <?php endforeach;?> 
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

                <!-- Inicio Tab_2 -->
                

              </div>
              <!-- nav-tabs-custom -->

              <div class="box-footer">
                <a class="btn btn-danger" href="<?php echo base_url(); ?>mantenimiento/cusuario/vlistaU">CANCELAR</a>
                <button type="submit" class="btn btn-info pull-right">GUARDAR</button>
              </div>
              </form>
            </div>
            <div class="box-body">

            </div>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-md-8 -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->