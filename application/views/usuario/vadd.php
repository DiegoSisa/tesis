<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      USUARIO
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <li class="active">Nuevo Usuario</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Default box -->
      <div class="col-md-8">
        <div class="box box-info col-md-6">
          <div class="box-header with-border">
            <h3 class="box-title">Datos Para nuevo Usuario</h3>
            <!-- Custom Tabs -->
            
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">DATOS PERSONALES</a></li>
                <li><a href="#tab_2" data-toggle="tab">DATOS DE LA CUENTA</a></li>

                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
              </ul>
              <form action="<?php echo base_url();?>mantenimiento/cusuario/guardar" method="post">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <!-- Inicio 1ra fila -->
                  <div class="row">

                    <div class="form-group col-md-6" <?php echo (form_error("cedula")) ? 'has-error' : ''; ?>>
                      <label for="cedula">Cedula</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        <input type="text" class="form-control" maxlength="10" placeholder="Cedula..." id="cedula" name="cedula" onkeyup="validarDocumento();" value="<?php echo set_value("cedula") ?>">
                        <span class="help-block"></span>
                      </div>
                      <?php echo form_error("cedula", "<span class='help-block'>", "</span>"); ?>
                    </div>


                    <div class="form-group col-md-6" >
                      <label for="exampleInputPassword1">Nombres</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Nombre..." name="txtNombres" onkeyup="mayus(this)">
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 1ra fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Apellidos</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Apellido..." name="txtApellido" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Telefono</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Telefono..." name="txtTelefono" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Direccion</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="	fa fa-map-marker"></i></span>
                        <input type="text" class="form-control" placeholder="Direccion..." name="txtDireccion" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Celular</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                        <input type="text" class="form-control" placeholder="Celular..." name="txtCelular" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Correo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                        <input type="text" class="form-control" placeholder="Email..." name="txtCorreo" onkeyup="minus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Sexo</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-female"></i></span>
                        <span class="input-group-addon"><i class="fa fa-male"></i></span>
                        <select name="sexo" id="sexo" class="form-control">
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6 <?php echo (form_error("fecha")) ? 'has-error' : ''; ?>">
                      <label for="fecha">Fecha de Nacimiento</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                        <input type="text" id="fecha" name="fecha" class="form-control" placeholder="aaaa-mm-dd"value="<?php echo set_value("fecha") ?>">
                      </div>
                      <?php echo form_error("fecha", "<span class='help-block'>", "</span>"); ?>
                    </div>

                   <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Rol</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <select class="form-control selecct2" style="width: 100%" name="txtrol" id="txtrol">
                        
                        <?php foreach ($roles as $rol):?> 

                             <option value="<?php echo $rol->ID_ROL?>"><?php echo $rol->NOMBRE_ROL?></option>
                          <?php endforeach;?> 
                        </select>
                      </div>
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

                <!-- Inicio Tab_2 -->
                <div class="tab-pane" id="tab_2">

                  <!-- Inicio 2da fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Usuario</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" placxeholder="Usuario..." name="usser">
                      </div>
                    </div>

                    <div class="form-group col-md-6">

                      <label for="textpassword">Contraseña</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" placeholder="Contraseña..." name="txtclave" id="txtclave">
                      </div><br>

                      <label for="textconfirm"> Verificar Contraseña</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" placeholder="Contraseña..." name="txtclave2" id="txtclave2" onkeyup="validacion('txtclave2')">
                      </div>
                      
                    </div>

                  </div>
                  <!-- Inicio 2da fila -->

                </div>
                <!-- FIn Tab_2 -->

              </div>
              <!-- nav-tabs-custom -->

              <div class="box-footer">
                <a class="btn btn-danger" href="<?php echo base_url(); ?>mantenimiento/cusuario/vlistaU">CANCELAR</a>
                <input type="submit" class="btn btn-info pull-right" value="Guardar">
              </div>
              </form>
            </div>
            <div class="box-body">

            </div>

          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-4">
          <?php if (validation_errors()) : ?>
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Errores!</h4>
              <?php echo validation_errors(); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->