<body>
    
    <!-- banner -->
    <div class="banner_top" id="home">
        <div data-vide-bg ="<?php echo base_url(); ?>assetscliente/video/gift-packs">
            <div class="center-container inner-container">
                <div class="w3_agile_header">
                    <div class="w3_agileits_logo">
                        <h1><a href="index.php">CAMILA HURTADO<span>Life is a gift</span></a></h1>
                    </div>
                    <div class="w3_menu">
                        <div class="agileits_w3layouts_banner_info">

                            <form action="#" method="post"> 
                                <input type="search" name="search" placeholder=" " required="">
                                <input type="submit" value="Search">
                            </form>
                        </div>
                        <div class="top-nav">
                            <nav class="navbar navbar-default">
                                <div class="container">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu						
                                    </button>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <li class="home-icon"><a href="<?php echo base_url(); ?>"><span class="fa fa-home" aria-hidden="true"></span></a></li>
                                        <li><a href="about.html">Acerca De</a></li>
                                        <li><a href="gallery.html">Galleria</a></li>
                                        <li><a href="<?php echo base_url(); ?>Ccarrito/index" class="active">Reserva</a></li>

                                        <!--<li><a href="contact.html">Contactos</a></li>-->
                                        <li class="nav-cart-w3ls">
                                            <a class="w3view-cart" href="<?php echo base_url(); ?>index.php/ccarrito/verCarrito"><label id="nro_carro" aria-hidden="true">0</label> <i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>
                                        </li>
                                        <li><a href="<?php echo base_url();?>auth/logout"  class="active">Salir</a></li>
                                    </ul>	
                                    <div class="clearfix"> </div>
                                </div>	
                            </nav>	
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- banner-text -->
                <h2 class="inner-heading-agileits-w3layouts">Bienvenido cliente</h2>
                <!--banner Slider starts Here-->
            </div>
        </div>
    </div>
    <!-- //banner -->
    <!--Buy-now -->
