<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/template/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('apellidos')." ".$this->session->userdata('nombres');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i>EN LINEA</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU DE NAVEGACION</li>
        <li  class="treeview
        <?php if(isset($active) ):?> 
             <?php  if($active =='usuarioActivo'):   ?>
                active
              <?php endif; ?>
        <?php endif;?> 
        ">
          <a href="#">
            <i class="fa fa-user"></i> <span>USUARIO</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url(); ?>mantenimiento/cusuario/vlistaU"></i>ADMINISTRAR USUARIOS</a></li>
          </ul>
        </li>


        <li 
        <?php if(isset($active) && $active ==='clienteActivo'):?> 
            class="treeview active"
            <?php else:?>
            class="treeview"
        <?php endif;?> 
        
        >
          <a href="#">
            <i class="fa fa-users"></i> <span>CLIENTES</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if(isset($active)&& $active=='clienteActivo'):?>  class="active" <?php endif;?>> <a href="<?php echo base_url(); ?>mantenimiento/ccliente/vlistarc"></i>ADMINISTRAR CLIENTES</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-tags"></i> <span>PIEZA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>mantenimiento/ctraje/vlistart"></i>ADMINISTRAR PIEZAS</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>ALQUILER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>mantenimiento/calquiler/valquiler"><i></i>NUEVO ALQUILER</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-reply"></i> <span>DEVOLUCIONES</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>mantenimiento/cdevolucion/vDevolucion"><i></i>ADMINISTRAR DEVOLUCIONES</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>CUENTAS POR COBRAR</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i ></i>REGISTRO DE CUENTAS POR COBRAR</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>REPORTES</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i></i>REPORTES DE ALQILER</a></li>
            <li><a href="../../index2.html"><i></i>REPORTES DE TRAJES</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Cliente</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Ccarrito/index"><i></i>RESERVA</a></li>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->