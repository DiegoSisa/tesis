
<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/validate.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>
<script>
    $(document).ready(function () {
        $("#formu").validate({
                rules: {
                    abonoL: {
                        required: true
                                //minlength: 5,
                                //maxlength: 10,
                                //email: true
                                //startWithA: true
                    },
                    id_alquiler: {
                        required: true
                    }

                }
            });
    });


</script>
<script>
    function mostrar_detalles(id) {
        $.ajax({
            type: 'GET',
            url: base_url + "mantenimiento/calquiler/listar_detalle/" + id,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var html = "";
                var i;
                //console.log("cliente "+data);
                for (i = 0; i < data.length; i++) {

                    html
                            += "<tr>"
                            + "<td>" + (i + 1) + "</td>"
                            + "<td>" + data[i].CANTIDAD_PIEZA + "</td>"
                            + "<td>" + data[i].DESCRIPCION_PIEZA + "</td>"
                            + "<td>" + data[i].TALLA_PIEZA + "</td>"
                            + "<td>" + data[i].COLOR_PIEZA + "</td>"
                            + "</tr>";

                }
                $("#detalles_piezas").html(html);
                console.log(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }

    function mostrar_reserva(id) {

        $.ajax({
            type: 'GET',
            url: base_url + "mantenimiento/calquiler/getReserva/" + id,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var html = "";
                var i;
                //console.log("cliente "+data);
                for (i = 0; i < data.length; i++) {
                    $("#id_alquiler").val(data[i].ID_ALQUILER);
                    $("#totalL").val(data[i].PRECIO_TOTAL);
                    $("#abonoL").val(data[i].PRECIO_TOTAL);
                    

                }
                //$("#detalles_piezas").html(html);
                //console.log(html);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }

</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Registrar</a></li>
            <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Listado</a></li>

        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">

                <section class="content-header">

                    <h1>
                        Alquiler
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                        <!--<li><a href="<?php echo base_url(); ?>mantenimiento/calquiler/guardar">Listar</a></li>-->
                        <li class="active">Nuevo Usuario</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box" id="btnAdd">
                                <a href="#" ><span class="info-box-icon bg-danger"><i class="fa fa-cart-arrow-down"></i></span></a>
                                <div class="">
                                    <span class="info-box-text">NUEVO ALQUILER</span>
                                    <span class="info-box-number"></span>
                                </div>

                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!--Barra de busqueda-->
                        <form name="formBusqueda" id="formBusqueda">
                            <div class="col-lg-4">
                                <div class="input-group">

                                    <input type="text" class="form-control" placeholder="Buscar cliente" name="txtbusqueda" id="txtbusqueda">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="button" name="buscar" id="buscar"><i class="fa fa-search"></i></button>
                                    </span>

                                </div>
                                <div id="Mensaje"></div>
                            </div>
                        </form>
                        <!-- /.col -->
                    </div>
                    <!-- Main row -->

                    <div class="row">
                        <!-- Default box -->
                        <div class="col-md-8">
                            <div class="box box-info col-md-6">
                                <div class="box-header with-border">

                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">DATOS DEL CLIENTE</a></li>


                                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                        </ul>
                                        <form action="<?php echo base_url(); ?>mantenimiento/calquiler/guardaAlquiler" method="post">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_1">
                                                    <!-- Inicio 1ra fila -->
                                                    <div class="row">


                                                        <input type="hidden" class="input-group" placeholder="Cedula..." name="idcliente" id="idcliente" readonly>


                                                        <div class="form-group col-md-6">
                                                            <label for="cedula">Cedula</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                <input type="text" class="form-control" placeholder="Cedula..." name="txtcedula" id="txtcedula" readonly>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label for="clientea">Cliente</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                                <input type="text" class="form-control" placeholder="Cliente..." name="txtcliente" id="txtcliente" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Fin 1ra fila -->

                                                    <!--Inicio 2da Fila-->
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="direccionn">Dirección</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-location-arrow"></i></span>
                                                                <input type="text" class="form-control" placeholder="Dirección..." name="txtdireccion" id="txtdireccion" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="correo">Correo</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                                                <input type="text" class="form-control" placeholder="Correo..." name="txtcorreo" id="txtcorreo" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Fin 2da Fila-->

                                                    <!--Inicio 3ra Fila-->
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="telefono">Telefono</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                <input type="text" class="form-control" placeholder="Telefono..." name="txttelefono" id="txttelefono" readonly>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label for="celular">Celular</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                <input type="text" class="form-control" placeholder="Celular..." name="txtcel" id="txtcel" readonly>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <!-- /.tab-pane -->
                                                </div>
                                                <h3> <span class="label label-info">Detalle Alquiler</span></h3>
                                                <button type="button" data-toggle="modal" data-target="#listaPiezas" id="agregar" name="agregar" class="btn btn-info pull-right"><i class="fa fa-cart-plus" ></i>  Agregar Pieza</button>

<!-- <input type='hidden' name='idpieza[]' value='"+#totalpieza+"'>-->
                                                <!-------Inicio de tabla -->
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Pieza</th>
                                                            <th>Cantidad</th>
                                                            <th>Existentes</th>
                                                            <th>Precio/U</th>
                                                            <th>Precio/T</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>



                                                    <tbody id="detalle">



                                                    </tbody>

                                                </table>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="txttotal">TOTAL</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                                            <input type="text" class="form-control" placeholder="0.00" name="txttotal" id="txttotal" readonly>
                                                        </div>



                                                        <label for="txtabono">ABONO</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                                            <input type="text" class="form-control" placeholder="0.00" name="txtabono" id="txtabono" onkeyup="calucularDirencia()" onkeypress="return numeros(event)">
                                                        </div>

                                                        <div id="msgabono"></div>

                                                        <label for="txtdiferencia">DIFERENCIA</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                                            <input type="text" class="form-control" placeholder="0.00" name="txtdiferencia" id="txtdiferencia" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="form-group col-md-6 <?php echo (form_error("fechares")) ? 'has-error' : ''; ?>">
                                                            <label for="fechares">Fecha de Alquiler</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                                                <input type="text" id="fechares" name="fechares" class="form-control" placeholder="aaaa-mm-dd"value="<?php echo set_value("fechares") ?>">
                                                            </div>
                                                            <?php echo form_error("fechares", "<span class='help-block'>", "</span>"); ?>
                                                        </div>

                                                        <div class="form-group col-md-6 <?php echo (form_error("fechaalq")) ? 'has-error' : ''; ?>">
                                                            <label for="fechaalq">Fecha de Devolucion</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                                                <input type="text" id="fechaalq" name="fechaalq" class="form-control" placeholder="aaaa-mm-dd"value="<?php echo set_value("fechaalq") ?>">
                                                            </div>
                                                            <?php echo form_error("fechaalq", "<span class='help-block'>", "</span>"); ?>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label for="txtdocumento">Documento</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                <input type="text" class="form-control" placeholder="Documento..." name="txtdocumento" id="txtdocumento" onkeyup="mayus(this)">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="txtobs">Observación</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-comment-o"></i></span>
                                                                <input type="text" class="form-control" placeholder="Observación..." name="txtobs" id="txtobs" onkeyup="mayus(this)">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="box-footer">
                                                        <a class="btn btn-danger" href="<?php echo base_url(); ?>mantenimiento/calquiler/valquiler">CANCELAR</a>
                                                        <button type="submit" id="btnguardar" name="btnguardar" class="btn btn-info pull-right">GUARDAR</button>
                                                    </div>   

                                                </div>
                                        </form>
                                    </div>


                                    <div class="box-body">

                                    </div>

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col-md-8 -->
                        </div>
                        <!-- /.row -->
                </section>
            </div>
            <!-- LISTADO DE ALQUILER-->
            <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">

                <section class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Lista de Alquileres y reservas</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table class="table no-margin">
                                            <thead>
                                            <td><b>N.</b></td>
                                            <td><b>Cedula</b></td>
                                            <td><b>Cliente</b></td>
                                            <td><b>F. Registro</b></td>
                                            <td><b>F. Retiro</b></td>                                            
                                            <td><b>Total</b></td>                                            
                                            <td><b>Bono</b></td>                                            
                                            <td><b>Saldo pendiente</b></td>                                            
                                            <td><b>Estado</b></td>
                                            <td><b>Acciones</b></td>
                                            </thead>
                                            <tbody id="data_alquiler">
                                                <?php
                                                $cont = 1;
                                                if (!empty($lista_alquileres)) :
                                                    ?>
                                                    <?php foreach ($lista_alquileres as $item) : ?>
                                                        <tr>
                                                            <td><?php echo $cont; ?></td>
                                                            <td><?php echo $item->CEDULA_PERSONA; ?></td>
                                                            <td><?php echo $item->NOMBRE_PERSONA . ' ' . $item->APELLIDO_PERSONA ?></td>
                                                            <td><?php echo $item->FECHA_RESERVA; ?></td>
                                                            <td><?php echo $item->FECHA_RETIRO; ?></td>
                                                            <td><?php echo $item->PRECIO_TOTAL; ?></td>
                                                            <td><?php echo $item->ABONO; ?></td>
                                                            <td><?php echo ($item->PRECIO_TOTAL - $item->ABONO); ?></td>
                                                            <td>
                                                                <span class='label label-danger'><?php echo $item->ESTADO_ALQUILER; ?></span>
                                                            </td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" data-toggle="modal" data-target="#listaDetalle" id="mostrar" onclick="return mostrar_detalles('<?php echo $item->ID_ALQUILER; ?>')" name="agregar" class="btn btn-info pull-right"><i class="fa fa-cart-plus" ></i>  Ver detalles</button>
                                                                    <?php if ($item->ESTADO_ALQUILER == 'R') { ?>
                                                                        <button type="button" data-toggle="modal" data-target="#levantar" id="mostrar" onclick="return mostrar_reserva('<?php echo $item->ID_ALQUILER; ?>')" name="agregar" class="btn btn-success pull-right"><i class="fa fa-cart-plus" ></i>  Levantar Reserva</button>
                                                                    <?php } ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $cont++; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div><!-- /example -->


    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include("listapieza.php");
include("detalle.php");
include("levantar.php");
?>
<script>
    function calucularDirencia() {
        var abono = parseFloat($("#txtabono").val());
        console.log(abono);
        var total = parseFloat($('#txttotal').val());
        if (abono !== "NaN") {
            if (abono <= total && abono > 0) {
                var diferencia = total - abono;
                $('#txtdiferencia').val(diferencia);
            } else {
                var menssaje = '<div class="alert alert-danger" style="font-size:10px">';
                menssaje += "EL pago no debe exeder al monto y NO ser menor a cero";
                menssaje += "</div>";
                $("#msgabono").show();
                $("#msgabono").html(menssaje);
                $("#msgabono").hide(6000);
            }
        }
        if (isNaN(parseFloat($("#txtabono").val()))) {
            $('#txtdiferencia').val("0.00");
        }
    }
</script>

