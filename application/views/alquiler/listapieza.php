<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="listaPiezas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header text-primary" style="background: #000000">
        <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Lista de Piezas <i
            class="fa fa-list"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
          <div id="Mensaje1"></div>
          <!-- /.card-body -->
          <div class="card-body">
            <table id="agregarArticulo" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nro</th>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Talla</th>
                  <th>Imagen</th>
                </tr>
              </thead>
              <tbody id="articulos">
                <?php
                $cont = 1;
                if (!empty($pieza)) : ?>
                <?php foreach ($pieza as $piezas) : ?>
                <tr>
                  <td><?php echo $cont; ?></td>
                  <td><?php echo $piezas->COD_PIEZA; ?></td>
                  <td><?php echo $piezas->DESCRIPCION_PIEZA; ?></td>
                  <td><?php echo $piezas->CANTIDAD_PIEZA; ?></td>
                  <td><?php echo $piezas->PRECIO_PIEZA; ?></td>
                  <td><?php echo $piezas->TALLA_PIEZA; ?></td>
                  <td><?php echo $piezas->FOTO_PIEZA; ?></td>
                  <td>
                    <div class="btn-group">
                      <button id="agregarp" name="agregarp" type="button" class="btn btn-info" data-toggle="tooltip" title="Agregar" onclick="agregarpieza('<?php echo $piezas->ID_PIEZA?>')"><i
                          class="fa fa-plus" ></i></button>
                    </div>
                  </td>
                </tr>
                <?php $cont++; ?>
                <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>