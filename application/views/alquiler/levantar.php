<!-- Modal -->
<!-- Content Wrapper. Contains page content -->

<div class="modal fade bs-example-modal-lg" id="levantar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header text-primary" style="background: #000000">
                <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Levantar Reserva <i
                        class="fa fa-list"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div id="Mensaje1"></div>
                    <!-- /.card-body -->
                    <div class="card-body">
                        <form id="formu" method="POST" action="<?php echo base_url(); ?>mantenimiento/calquiler/levantarReserva">
                            <input type="hidden" name="id_alquiler" id="id_alquiler">
                            <div class="form-group">
                                <label for="totalL" class="col-sm-2 control-label">Total:</label>                                
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="totalL" id="totalL" placeholder="Ingrese el abono" readonly>                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="abonoL" class="col-sm-2 control-label">Abono:</label>                                
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="abonoL" id="abonoL" placeholder="Ingrese el abono" >                                    
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
                                <button type="submit" class="btn btn-success pull-right">Levantar reserva</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </div>
</div>