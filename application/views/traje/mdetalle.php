<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="detalleP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header text-primary" style="background: #032c80">
        <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle"><b>Detalle de la Pieza</b><i
            class="fa fa-list"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
          <div id="Mensaje2"></div>
          <!-- /.card-body -->
          <div class="card-body">
            <table id="detallePieza" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Talla</th>
                  <th>Foto</th>
                  <th>Editar</th>
                </tr>
              </thead>
              <tbody id="articulos">
                <?php

                if (!empty($pieza)) : ?>
                <?php foreach ($pieza as $piezas) : ?>
                <tr>
                  <td><?php echo $piezas->COD_PIEZA; ?></td>
                  <td><?php echo $piezas->DESCRIPCION_PIEZA; ?></td>
                  <td><?php echo $piezas->CANTIDAD_PIEZA; ?></td>
                  <td><?php echo $piezas->PRECIO_PIEZA; ?></td>
                  <td><?php echo $piezas->TALLA_PIEZA; ?></td>
                  <td><?php echo $piezas->FOTO_PIEZA; ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-info" data-toggle="tooltip" title="Editar" onclick="agregarpieza('<?php echo $piezas->ID_PIEZA?>')"><i
                          class="fa fa-edit" ></i></button>
                    </div>
                  </td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>