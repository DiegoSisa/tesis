<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      EDITAR DATOS DE LA PIEZA
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <!--<li><a href="<?php echo base_url();?>mantenimiento/cpaciente/index">Listar</a></li>-->
      <li class="active"></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Default box -->
      <div class="col-md-8">
        <div class="box box-info col-md-6">
          <div class="box-header with-border">
            
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">DATOS DE LA NUEVA PIEZA</a></li>
                

                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
              </ul>
              <form action="<?php echo base_url();?>mantenimiento/ctraje/guardar" method="post" enctype="multipart/form-data">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <!-- Inicio 1ra fila -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="txtcod">Codigo De La Pieza</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        <input type="text" class="form-control" placeholder="Codigo..." name="txtcod" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="txttalla">Talla</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Talla..." name="txttalla" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>
                  <!-- Fin 1ra fila -->

                  <!--Inicio 2da Fila-->
                  <div class="row">
                    <div class="form-group col-md-12">
                      <label for="txtdescripcion">Descripcion</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                        <input type="text" class="form-control" placeholder="Descripcion..." name="txtdescripcion" onkeyup="mayus(this)">
                      </div>
                    </div>
                  </div>
                  <!--Fin 2da Fila-->

                  <!--Inicio 3ra Fila-->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="precio">Precio</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        <input type="text" class="form-control" placeholder="Precio..." name="precio" id="precio" onkeypress="return numeros(event)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="cantidad">Cantidad</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                        <input type="number" class="form-control" name="cantidad" id="cantidad" placeholder="0" onkeypress="return numeros(event)">
                      </div>
                    </div>
                  </div>

                  <!-- /.tab-pane -->

                  <!--Inicio 3ra Fila-->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="txtcolor">Color</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-dashboard"></i></span>
                        <input type="text" class="form-control" placeholder="Color..." name="txtcolor" id="txtcolor" onkeyup="mayus(this)">
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="fotoPieza" >Imagen Pieza</label>
                      <div class="input-group">
                        <input type="file" name="fotoPieza" id="fotoPieza">
                      </div>
                    </div>
                  </div>

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

              </div>
              <!-- nav-tabs-custom -->

              <div class="box-footer">
                <a class="btn btn-danger" href="<?php echo base_url(); ?>mantenimiento/ctraje/vlistart">CANCELAR</a>
                <button type="submit" class="btn btn-info pull-right">GUARDAR</button>
              </div>
              </form>
            </div>
            <div class="box-body">

            </div>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-md-8 -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->