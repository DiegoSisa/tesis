<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            CLIENTES REGISTRADOS
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box" id="btnAdd">
                    <a href="#" data-toggle="modal" data-target="#myModalc"><span class="info-box-icon bg-aqua"><i class="fa fa-user-o"></i></span></a>
                    <div class="info-box-content">

                        <span class="info-box-text">NUEVO CLIENTE</span>
                        <span class="info-box-number"></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- Main row -->

        <!-- MOSTRAR MENSAJE -->
        <div class="alert alert-success" style="display: none;">

        </div>

        <div class="alert alert-danger" style="display: none;">

        </div>

        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
                <!-- MAP & BOX PANE -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de Clientes</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <td><b>N.</b></td>
                                <td><b>Nombre y Apellido</b></td>
                                <td><b>Cedula</b></td>
                                <td><b>Estado</b></td>
                                <td><b>Acciones</b></td>
                                </thead>
                                <tbody id="showdata">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <div class="modal fade" id="myModalc" tabindex="-1" role="dialog" aria-labelledby="modalcliente" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">DATOS DEL CLIENTE</h4>
                        </div>
                        <div class="modal-body">
                            <!-- form start -->
                            <form class="form-horizontal" id="myFormc" method="post" action="<?php echo base_url(); ?>mantenimiento/ccliente/guardarc">
                                <div class="box-body">
                                <!-- <input type="hidden" name="idEsp" value="0">-->

                                    <div class="form-group">
                                        <label for="cedula" class="col-sm-2 control-label">Cedula:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cedula..." onkeypress="return numeros(event)" maxlength="10" onkeyup="validarDocumento();">
                                            <?php echo form_error("cedula", "<span class='help-block'>", "</span>"); ?>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtnombre" class="col-sm-2 control-label">Nombre:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txtnombre" id="txtnombre" placeholder="Nombre..." onkeyup="mayus(this)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtapellido" class="col-sm-2 control-label">Apellido:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txtapellido" id="txtapellido" placeholder="Apellido..." onkeyup="mayus(this)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtdireccion" class="col-sm-2 control-label">Direccion:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txtdireccion" id="txtdireccion" placeholder="Direccion..." onkeyup="mayus(this)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txttelefono" class="col-sm-2 control-label">Telefono:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txttelefono" id="txttelefono" placeholder="Telefono..." onkeypress="return numeros(event)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtcelular" class="col-sm-2 control-label">Celular:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txtcelular" id="txtcelular" placeholder="Celular..." onkeypress="return numeros(event)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtcorreo" class="col-sm-2 control-label">Correo:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="txtcorreo" id="txtcorreo" placeholder="Correo..." onkeyup="minus(this)">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtsexo" class="col-sm-2 control-label">Sexo</label>
                                        <div class="col-sm-10">
                                            <select name="txtsexo" id="txtsexo" class="form-control">
                                                <option value="M">Masculino</option>
                                                <option value="F">Femenino</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group <?php echo (form_error("fechaCli")) ? 'has-error' : ''; ?>">
                                        <label for="fechaCli" class="col-sm-2 control-label">Fecha de Nacimiento:</label>
                                        <div class="col-sm-10">

                                            <input type="text" id="fechaCli" name="fechaCli" class="form-control" placeholder="aaaa-mm-dd"value="<?php echo set_value("fechaCli") ?>">
                                        </div>
                                        <?php echo form_error("fechaCli", "<span class='help-block'>", "</span>"); ?>
                                    </div>

                                    <!--<div class="form-group"> 
                                  </div>-->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                                        <button type="submit" class="btn btn-success pull-right">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
